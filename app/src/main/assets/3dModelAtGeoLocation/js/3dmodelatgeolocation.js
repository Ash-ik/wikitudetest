var World = {
	loaded: false,
	rotating: false,

	init: function initFn() {
		this.createModelAtLocation();

	},

	createModelAtLocation: function createModelAtLocationFn() {

		/*
			First a location where the model should be displayed will be defined. This location will be relativ to the user.	
		*/
		var location = new AR.GeoLocation(23.8016182, 90.3607413);
        var labelLocation = new AR.RelativeLocation(null, 5, 0, 1);
		/*
			Next the model object is loaded.
		*/
		var modelEarth = new AR.Model("assets/earth.wt3", {
			onLoaded: this.worldLoaded,
			scale: {
				x: 1,
				y: 1,
				z: 1
			},
			onClick:function() {
    			modelEarth.enabled = false;
    			var emojiImage = new AR.ImageResource("assets/emoji.png");
                var hiddenObject = new AR.ImageDrawable(emojiImage, 1, {
                translate: {
                    x: -0.1,
                    y: -0.275
                    },
                    scale: {
                    x:2.0,
                    y:2.0
                    }
                });
            var newObj = new AR.GeoObject(location, {
                drawables: {
                cam: [hiddenObject]
                    }
//,onExitFieldOfVision :
//                    function()
//                    { modelEarth.enabled = true;
//                      newObj.enabled = false; }
                });
    	}
		});

        this.rotateAnimation = new AR.PropertyAnimation(modelEarth, "rotate.heading", 0, 360, 8000);
        var label = new AR.Label("Click On The Box To Open", 20, {
  translate : { x: 1 },
  onClick : function() {
    label.text += "CLICK "
  },
  verticalAnchor : AR.CONST.VERTICAL_ANCHOR.TOP,
  opacity : 0.1
});

        var indicatorImage = new AR.ImageResource("assets/indi.png");

        var indicatorDrawable = new AR.ImageDrawable(indicatorImage, 0.1, {
            verticalAnchor: AR.CONST.VERTICAL_ANCHOR.TOP
        });

		/*
			Putting it all together the location and 3D model is added to an AR.GeoObject.
		*/
		var obj = new AR.GeoObject(location, {
            drawables: {
               cam: [modelEarth],
               indicator: [indicatorDrawable]
            }
        });
	},

	worldLoaded: function worldLoadedFn() {
		World.loaded = true;
		  World.rotateAnimation.start(-1);
		var e = document.getElementById('loadingMessage');
		e.parentElement.removeChild(e);
	}
};

World.init();
