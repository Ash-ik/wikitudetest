var World = {
loaded: false,
rotating: false,
created: false,

init: function initFn() {
// wait for first location signal before creating the experience to ensure you know user's location during setup process
AR.context.onLocationChanged = function(latitude, longitude, altitude, accuracy){
// store user's location so you have access to it at any time
World.myLocation = {"latitude": latitude, "longitude" : longitude, "altitude" : altitude };
//document.getElementById('fetchingLocation').innerHTML = (World.myLocation.latitude + ", " + World.myLocation.longitude);

if (!World.created) {
World.created = true;
World.createModelAtLocation();
}
};
},

createModelAtLocation: function createModelAtLocationFn() {
// place object around the user, in this case just a few meters next to the user. Note that the object will stay where it is, so user can move around it.
var geoLocation = new AR.GeoLocation(World.myLocation.latitude + 0.00001, World.myLocation.longitude + 0.00001, AR.CONST.UNKNOWN_ALTITUDE);

/*
you may also use relativeLocation using null as first param to display an object relative to the user. This sample uses GeoLocation to show that it also works without Relative locations
*/
// var location = new AR.RelativeLocation(null, 1, 1, 0);

 /*
Next the model object is loaded.
*/

var modelEarth = new AR.Model("assets/earth.wt3", {
onLoaded: this.worldLoaded,
scale: {
x: 0.0003,
y: 0.0003,
z: 0.0003
}
});

var indicatorImage = new AR.ImageResource("assets/indi.png");
var indicatorDrawable = new AR.ImageDrawable(indicatorImage, 0.1, {
     verticalAnchor: AR.CONST.VERTICAL_ANCHOR.TOP
});

/*
Putting it all together the location and 3D model is added to an AR.GeoObject.
*/

var obj = new AR.GeoObject(geoLocation, {
    drawables: {
         cam: ,
        indicator:
       }
});
},

worldLoaded: function worldLoadedFn() {
World.loaded = true;
var e = document.getElementById('loadingMessage');
e.parentElement.removeChild(e);
}
};

World.init();