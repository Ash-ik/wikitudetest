var allCurrentModels = [];
var defaultScaleValue = 0.045;
var defaultRotationValue = 0;

var rotationValues = [];
var scaleValues = [];

var allCurrentCircles = [];

var oneFingerGestureAllowed = false;

var fixedLat = 0.0;
var fixedLon = 0.0;
var positionNotUpdated = true;
var show3dModels = true;
var World = {

    init: function initFn() {
        AR.logger.activateDebugMode();
        this.createOverlays();
        AR.context.onLocationChanged = function (lat, lon, alt, acc) {
            if(positionNotUpdated) {
                AR.logger.debug(lat + " " + lon);
                fixedLat = lat;
                fixedLon = lon;

                var userLocation = new AR.GeoLocation(fixedLat, fixedLon);
                var actionRange = new AR.ActionRange(userLocation, 100, {
                    onEnter : function() {
//                        actionRange2.enabled = false; //an ActionArea which can only be entered once
                              AR.logger.debug("Entered users current region");
                              show3dModels = true;
                              this.createOverlays();
                    },
                    onExit : function() {
                            AR.logger.debug("Exited users current region");
                            show3dModels = false;
                            this.createOverlays();
                    }
                });
                positionNotUpdated = false;
            }
            //else AR.logger.debug("Already setup done");

        };
    },

    createOverlays: function createOverlaysFn() {
//        AR.logger.debug(show3dModels);
        if(show3dModels){
//        AR.logger.debug("Show 3D Models");
        var crossHairsRedImage = new AR.ImageResource("assets/crosshairs_red.png");
                var crossHairsRedDrawable = new AR.ImageDrawable(crossHairsRedImage, 1.0);

                var crossHairsBlueImage = new AR.ImageResource("assets/crosshairs_blue.png");
                var crossHairsBlueDrawable = new AR.ImageDrawable(crossHairsBlueImage, 1.0);

                this.tracker = new AR.InstantTracker({
                    onChangedState:  function onChangedStateFn(state) {
                        // react to a change in tracking state here
                    },
                    // device height needs to be as accurate as possible to have an accurate scale
                    // returned by the Wikitude SDK
                    deviceHeight: 1.0,
                    onError: function(errorMessage) {
                        alert(errorMessage);
                    }
                });

                this.instantTrackable = new AR.InstantTrackable(this.tracker, {
                    drawables: {
                        cam: crossHairsBlueDrawable,
                        initialization: crossHairsRedDrawable
                    },
                    onTrackingStarted: function onTrackingStartedFn() {
                        // do something when tracking is started (recognized)
                    },
                    onTrackingStopped: function onTrackingStoppedFn() {
                        // do something when tracking is stopped (lost)
                    },
                    onTrackingPlaneClick: function onTrackingPlaneClickFn(xpos, ypos) {
                        // xPos and yPos are the intersection coordinates of the click ray and the
                        // instant tracking plane. they can be applied to the transform component
                        // directly
                        if(allCurrentModels.length == 0)
                            World.addModel(xpos, ypos);
                    },
                    onError: function(errorMessage) {
                        alert(errorMessage);
                    }
                });
        }
    },

    changeTrackerState: function changeTrackerStateFn() {
        
        if (this.tracker.state === AR.InstantTrackerState.INITIALIZING) {
            
            document.getElementById("tracking-start-stop-button").src = "assets/buttons/stop.png";
            document.getElementById("tracking-height-slider-container").style.visibility = "hidden";
            
            this.tracker.state = AR.InstantTrackerState.TRACKING;
        } else {

            document.getElementById("tracking-start-stop-button").src = "assets/buttons/start.png";
            document.getElementById("tracking-height-slider-container").style.visibility = "visible";
            
            this.tracker.state = AR.InstantTrackerState.INITIALIZING;
        }
    },
    
    changeTrackingHeight: function changeTrackingHeightFn(height) {
        this.tracker.deviceHeight = parseFloat(height);
    },
    
    isTracking: function isTrackingFn() {
        return (this.tracker.state === AR.InstantTrackerState.TRACKING);
    },

    addModel: function addModelFn(xpos, ypos, modelIndex) {
        if (World.isTracking()) {
            var model = new AR.Model("assets/models/earth.wt3", {
                scale: {
                    x: 0.8,
                    y: 0.8,
                    z: 0.8
                },
                translate: {
                    x: xpos,
                    y: ypos
                },
                rotate: {
                    // create with a random rotation to provide visual variety
                    z: Math.random() * 360.0
                },
                onDragBegan: function(x, y) {
                                    oneFingerGestureAllowed = true;
                                },
                                onDragChanged: function(relativeX, relativeY, intersectionX, intersectionY) {
                                    if (oneFingerGestureAllowed) {
                                        // We recommend setting the entire translate property rather than
                                        // its individual components as the latter would cause several
                                        // call to native, which can potentially lead to performance
                                        // issues on older devices. The same applied to the rotate and
                                        // scale property
                                        this.translate = {x:intersectionX, y:intersectionY};
                                    }
                                },
                                onDragEnded: function(x, y) {
                                    // react to the drag gesture ending
                                },
                                onRotationBegan: function(angleInDegrees) {
                                    // react to the rotation gesture beginning
                                },
                                onRotationChanged: function(angleInDegrees) {
                                },
                                onRotationEnded: function(angleInDegrees) {
                                },
                                onScaleBegan: function(scale) {
                                    // react to the scale gesture beginning
                                },
                                onScaleChanged: function(scale) {
                                },
                                onScaleEnded: function(scale) {
                                },onClick:function() {
                                        alert("Hidden Message");
                                     	}
            })

            allCurrentModels.push(model);
            this.instantTrackable.drawables.addCamDrawable(model);
        }
    },

    resetModels: function resetModelsFn() {
        for (var i = 0; i < allCurrentModels.length; i++) {
            this.instantTrackable.drawables.removeCamDrawable(allCurrentModels[i]);
        }
        allCurrentModels = [];
    }
};

World.init();
