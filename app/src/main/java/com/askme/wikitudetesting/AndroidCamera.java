package com.askme.wikitudetesting;

import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class AndroidCamera extends Activity implements SurfaceHolder.Callback {


    private View viewControl;

    private ImageButton openGalleryButton;
    private ImageButton openDrawerButton;
    private ImageButton openMapButton;
    private ImageButton openFriendsButton;
    private ImageButton captureImageButton;

    private ImageButton menuButton;
    private ImageButton crossButton;
    private ImageButton flashButton;

    private ImageButton duration3hrButton;
    private ImageButton duration6hrButton;
    private ImageButton duration12hrButton;

    private GridView stickerGridView;
    private RelativeLayout topMenuBar;
    private RelativeLayout rightMenuBar;
    private RelativeLayout bottomMenuBar;


    Camera camera;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    boolean previewing = false;
    LayoutInflater controlInflater = null;
    boolean drawerOpened = false;
    boolean flashOn = false;

    int windowWidth = 0;
    int windowHeight = 0;
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        windowWidth = getWindowManager().getDefaultDisplay().getWidth();
        windowHeight = getWindowManager().getDefaultDisplay().getHeight();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        getWindow().setFormat(PixelFormat.UNKNOWN);

        surfaceView = findViewById(R.id.camerapreview);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        controlInflater = LayoutInflater.from(getBaseContext());
        viewControl = controlInflater.inflate(R.layout.control, null);
        LayoutParams layoutParamsControl = new LayoutParams(
                LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
        this.addContentView(viewControl, layoutParamsControl);

        initViews();

        stickerGridView.setAdapter(new ImageAdapter(this));

        setupListeners();
    }

    private void initViews() {
        openGalleryButton = viewControl.findViewById(R.id.gallery_button);
        openDrawerButton = viewControl.findViewById(R.id.drawer_button);
        openFriendsButton = viewControl.findViewById(R.id.friends_button);
        openMapButton = viewControl.findViewById(R.id.map_button);
        captureImageButton = viewControl.findViewById(R.id.capture_button);

        menuButton = viewControl.findViewById(R.id.menu_button);
        flashButton = viewControl.findViewById(R.id.flash_on_button);
        crossButton = viewControl.findViewById(R.id.cross_button);

        duration3hrButton = viewControl.findViewById(R.id.three_hr_button);
        duration6hrButton = viewControl.findViewById(R.id.six_hr_button);
        duration12hrButton = viewControl.findViewById(R.id.twelve_hr_button);


        stickerGridView = viewControl.findViewById(R.id.sticker_gallery_grid);
        topMenuBar = viewControl.findViewById(R.id.topMenuBar);
        rightMenuBar = viewControl.findViewById(R.id.rightMenuBar);
        bottomMenuBar = viewControl.findViewById(R.id.bottomMenuBar);
    }

    private void setupListeners() {

        openMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidCamera.this,FriendsOnMapActivity.class));
            }
        });

        openFriendsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AndroidCamera.this,MainActivity.class));
            }
        });

        flashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(flashOn == true){
                    Camera.Parameters params = camera.getParameters();
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(params);
                    flashButton.setImageResource(R.drawable.flash_off_icon);
                }else {
                    Camera.Parameters params = camera.getParameters();
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(params);
                    flashButton.setImageResource(R.drawable.flash_on_icon);
                }
                flashOn = !flashOn;
            }
        });
        openGalleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(
                        "content://media/internal/images/media/"));
                startActivity(intent);
            }
        });

        openDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawerOpened == false) {
                    openRightDrawer();
                    openBottomDrawer();
                } else {
                    closeRightDrawer();
                    closeBottomDrawer();
                }
                drawerOpened = !drawerOpened;
            }
        });

        stickerGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //drawStickerOnCameraSurface(i);
            }
        });
    }

    private void drawStickerOnCameraSurface(int stickerIndex) {

        if(surfaceHolder == null)
            return;

        Bitmap sticker = null;

        if(stickerIndex == 0) {
            sticker = BitmapFactory.decodeResource(getResources(),R.drawable.iron_man_sticker);
        }
        else {
            sticker = BitmapFactory.decodeResource(getResources(),R.drawable.po_sticker);
        }
        sticker = Bitmap.createBitmap(sticker,0,0,400,400);
        Canvas canvas = surfaceHolder.lockCanvas();
        canvas.drawBitmap(sticker,0,0,null);
        surfaceHolder.unlockCanvasAndPost(canvas);
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
        // TODO Auto-generated method stub
        if (previewing) {
            camera.stopPreview();
            previewing = false;
        }

        if (camera != null) {
            try {
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                previewing = true;
                this.surfaceHolder = surfaceHolder;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        camera = Camera.open();
        camera.setDisplayOrientation(90);

        //set camera to continually auto-focus
        Camera.Parameters params = camera.getParameters();
        params.setRotation(90);
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        camera.setParameters(params);

        this.surfaceHolder = surfaceHolder;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        camera.stopPreview();
        camera.release();
        camera = null;
        previewing = false;

        this.surfaceHolder = surfaceHolder;
    }
    private void openRightDrawer(){
        rightMenuBar.setVisibility(View.VISIBLE);
        Animation animation =
                AnimationUtils.loadAnimation(this,
                        R.anim.slide_in_from_left);

        rightMenuBar.startAnimation(animation);

    }
    private void closeRightDrawer(){
        Animation animation =
                AnimationUtils.loadAnimation(this,
                        R.anim.slide_out_to_right);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                rightMenuBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rightMenuBar.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        rightMenuBar.startAnimation(animation);
    }

    private void openBottomDrawer(){
        stickerGridView.setVisibility(View.VISIBLE);
        Animation animation =
                AnimationUtils.loadAnimation(this,
                        R.anim.slide_in_from_bottom);

        bottomMenuBar.startAnimation(animation);
        stickerGridView.startAnimation(animation);
    }
    private void closeBottomDrawer(){
        Animation animation =
                AnimationUtils.loadAnimation(this,
                        R.anim.slide_out_to_bottom);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                stickerGridView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                stickerGridView.setVisibility(View.GONE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        bottomMenuBar.startAnimation(AnimationUtils.loadAnimation(this,
                R.anim.slide_out_to_bottom));
        stickerGridView.startAnimation(animation);
    }
}