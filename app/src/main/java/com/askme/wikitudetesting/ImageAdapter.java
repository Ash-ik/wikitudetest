package com.askme.wikitudetesting;

/**
 * Created by FahimAlMahmud on 10/15/2017.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;

    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.iron_man_sticker, R.drawable.po_sticker
    };

    // Constructor
    public ImageAdapter(Context c){
        mContext = c;
    }

    @Override
    public int getCount() {
        return mThumbIds.length;
    }

    @Override
    public Object getItem(int position) {
        return mThumbIds[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {
            gridView = new View(mContext);

            // get layout from custom_gridview.xml
            gridView = inflater.inflate(R.layout.custom_gridview, null);

            // set value into imageview
            ImageView imageContent = gridView.findViewById(R.id.sticker_content);
            ImageView imageBorder = gridView.findViewById(R.id.sticker_border);
            imageContent.setImageResource(mThumbIds[position]);
        } else {
            gridView = convertView;
        }

        return gridView;
    }

}
